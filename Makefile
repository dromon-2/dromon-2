graphviz_files := $(shell find . -iname '*.dot')


$(graphviz_files:.dot=.png): %.png: %.dot
	dot -Tpng -o $@ $<

all: $(graphviz_files:.dot=.png)
